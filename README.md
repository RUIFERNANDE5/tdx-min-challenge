# Talkdesk Apps List Mini-challenge solution by [Rui Fernandes](https://www.linkedin.com/in/ruifernande5/)

## Available Scripts

In the project directory, you can run:

### `npm/yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm/yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm/yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information. <br />

### `.env`
If you wish to change the default port create a `.env` file and configure its `PORT` value as shown in `.env.example`.  <br /><br />


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).