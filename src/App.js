import React from 'react';
import { AppsProvider } from './Store';
import CategoriesList from './components/Categories';
import AppsList from './components/List';
import Pagination from './components/Pagination';
import './App.css';

const App = () => {
  return (
    <div className="App">
      <AppsProvider>
        <div className="flex-container">
          <CategoriesList />
          <AppsList />
          <Pagination />
        </div>
      </AppsProvider>
    </div>
  );
};

export default App;
