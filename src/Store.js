import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v1';
import appsData from './apps.json';
import { prettifyUrl } from './utils';

const defaultPagination = {
  perPage: 3,
  totalPages: 1,
  currentPage: 1,
};

const AppsContext = React.createContext();
const AppsProvider = props => {
  const [pagination, setPagination] = useState(defaultPagination);
  const [apps, setApps] = useState([]);
  const [orderedApps, setOrderedApps] = useState([]);
  const [categories, setCategories] = useState([]);
  const [currentCategory, setCurrentCategory] = useState(null);
  const [searchTerm, setSearch] = useState('');

  const fetchApps = () => {
    const items = appsData;
    const subscriptionsAgg = items.map(item => {
      let subscriptionsTotal = 0;

      item.subscriptions.forEach(subscription => {
        subscriptionsTotal += parseInt(subscription.price, 10);
      });

      return {
        ...item,
        subscriptionsTotal,
      };
    });

    const orderedApps = subscriptionsAgg.sort((a, b) => {
      if (a.subscriptionsTotal < b.subscriptionsTotal) {
        return -1;
      }

      if (a.subscriptionsTotal > b.subscriptionsTotal) {
        return 1;
      }

      return 0;
    });

    const totalPages = Math.ceil(orderedApps.length / pagination.perPage);
    setPagination({
      ...pagination,
      totalPages: totalPages > 0 ? totalPages : 1,
    });

    setOrderedApps(orderedApps);

    setApps(
      orderedApps.slice(
        pagination.perPage * (pagination.currentPage - 1),
        pagination.perPage * pagination.currentPage
      )
    );
  };

  const searchApps = () => {
    let filteredApps = [];
    if (currentCategory) {
      filteredApps = orderedApps.filter(
        apps =>
          apps.categories.includes(currentCategory.label) &&
          apps.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    } else {
      filteredApps = orderedApps.filter(apps =>
        apps.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    const totalPages = Math.ceil(filteredApps.length / pagination.perPage);
    setPagination({
      ...pagination,
      totalPages: totalPages > 0 ? totalPages : 1,
    });

    setApps(
      filteredApps.slice(
        pagination.perPage * (pagination.currentPage - 1),
        pagination.perPage * pagination.currentPage
      )
    );
  };

  const fetchCategories = () => {
    const items = appsData;

    const categories = items.reduce((acc, item) => {
      item.categories.forEach(category => {
        if (!acc.some(c => c.label === category)) {
          acc.push({
            id: uuid(),
            label: category,
            slug: prettifyUrl(category),
          });
        }
      });

      return acc;
    }, []);

    const orderedCategories = categories.sort((a, b) => {
      if (a.label < b.label) {
        return -1;
      }
      if (a.label > b.label) {
        return 1;
      }
      return 0;
    });

    setCategories(orderedCategories);
  };

  useEffect(() => {
    searchApps();
  }, [searchTerm, currentCategory, pagination.currentPage]);

  useEffect(() => {
    fetchCategories();
    fetchApps();
  }, []);

  return (
    <AppsContext.Provider
      value={{
        apps,
        categories,
        pagination,
        searchTerm,
        setPagination,
        currentCategory,
        setCurrentCategory,
        setSearch,
      }}
    >
      {props.children}
    </AppsContext.Provider>
  );
};

AppsProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
    .isRequired,
};

const AppsConsumer = AppsContext.Consumer;
export { AppsProvider, AppsConsumer, AppsContext };
