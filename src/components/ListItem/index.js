import React from 'react';
import PropTypes from 'prop-types';

const ListItem = props => {
  const { id, name, description, categories, subscriptions } = props;

  return (
    <li key={id}>
      <div className="app-item">
        <div className="box-info">
          <div className="box-info--content">
            <div className="description">
              <h1>{name}</h1>
              <p>{description}</p>
            </div>
            <div className="tags">
              {categories.map((category, i) => (
                <span key={i}>
                  {category} {categories.length && i + 1 < categories.length && ' / '}
                </span>
              ))}
            </div>
          </div>
          <div className="box-info--footer">
            <ul>
              {subscriptions.map((subscription, index) => (
                <li key={index}>
                  <span>{subscription.name}</span>
                  <h3>
                    {subscription.price > 0 ? (
                      <>
                        {(subscription.price / 100).toFixed(2)}
                        <sup>€</sup>
                      </>
                    ) : (
                      <>Free</>
                    )}
                  </h3>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </li>
  );
};

ListItem.defaultProps = {
  description: '',
  categories: [],
  subscriptions: [],
};

ListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  categories: PropTypes.arrayOf(PropTypes.string),
  subscriptions: PropTypes.arrayOf(PropTypes.object),
};

export default ListItem;
