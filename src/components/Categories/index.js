import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { AppsContext } from '../../Store';
import CategoryItem from '../CategoryItem';

const CategoriesList = () => {
  const appsContext = useContext(AppsContext);
  const {
    categories,
    currentCategory,
    pagination,
    setCurrentCategory,
    setPagination,
  } = appsContext;

  const handleClick = (ev, category, current) => {
    ev.preventDefault();
    setCurrentCategory(!current ? category : null);
    setPagination({ ...pagination, currentPage: 1 });
  };

  return (
    <nav className="nav-categories">
      <h2>Categories</h2>
      {categories.length > 0 && (
        <ul className="nav-menu">
          {categories.map(category => {
            const current = currentCategory
              ? category.slug === currentCategory.slug
              : false;
            return (
              <CategoryItem
                key={category.id}
                category={category}
                current={current}
                callback={e => handleClick(e, category, current)}
              />
            );
          })}
        </ul>
      )}
    </nav>
  );
};

CategoriesList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
};

CategoriesList.defaultProps = {
  items: [],
};

export default CategoriesList;
