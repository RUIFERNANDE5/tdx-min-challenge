import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { AppsContext } from '../../Store';
import ListItem from '../ListItem';
import SearchBar from '../SearchBar';

const AppsList = () => {
  const appContext = useContext(AppsContext);
  const { apps } = appContext;

  return (
    <section className="apps-list">
      <SearchBar />
      {apps.length ? (
        <ul>
          {apps.map(app => (
            <ListItem key={app.id} {...app} />
          ))}
        </ul>
      ) : (
        <p style={{ textAlign: 'center', paddingTop: '4rem' }}>
          Sorry, no apps were found :(
        </p>
      )}
    </section>
  );
};

AppsList.propTypes = {
  items: [],
  searchTerm: '',
};

AppsList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
  search: PropTypes.func.isRequired,
  searchTerm: PropTypes.string,
};

export default AppsList;
