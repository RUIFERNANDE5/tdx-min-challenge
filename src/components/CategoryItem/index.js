import React from 'react';
import PropTypes from 'prop-types';

const CategoryItem = props => {
  const { category, current, callback } = props;
  const className = current ? 'active' : null;

  return (
    <li key={category.id} className={className}>
      <a href={category.slug} onClick={callback}>
        {category.label}
      </a>
    </li>
  );
};

CategoryItem.defaultProps = {
  current: false,
};

CategoryItem.propTypes = {
  category: PropTypes.object.isRequired,
  current: PropTypes.bool,
  callback: PropTypes.func.isRequired,
};

export default CategoryItem;
