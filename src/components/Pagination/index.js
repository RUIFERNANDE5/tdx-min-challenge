import React, { useContext } from 'react';
import { AppsContext } from '../../Store';

const Pagination = () => {
  const appsContext = useContext(AppsContext);
  const { pagination, setPagination } = appsContext;

  const handleOnClick = nextPage => {
    setPagination({
      ...pagination,
      currentPage: nextPage,
    });
  };

  return (
    <ul className="pagination">
      <li>
        <button
          onClick={() => handleOnClick(pagination.currentPage - 1)}
          disabled={pagination.currentPage <= 1}
        >
          &lt;
        </button>
      </li>
      {[...Array(pagination.totalPages)].map((page, index) => {
        const className = pagination.currentPage === index + 1 ? 'active' : '';
        return (
          <li key={index} className={className}>
            <button onClick={() => handleOnClick(index + 1)}>{index + 1}</button>
          </li>
        );
      })}
      <li>
        <button
          onClick={() => handleOnClick(pagination.currentPage + 1)}
          disabled={pagination.currentPage >= pagination.totalPages}
        >
          &gt;
        </button>
      </li>
    </ul>
  );
};

export default Pagination;
