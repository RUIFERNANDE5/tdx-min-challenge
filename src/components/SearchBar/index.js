import React, { useContext } from 'react';
import { AppsContext } from '../../Store';

const SearchBar = () => {
  const appContext = useContext(AppsContext);
  const { searchTerm, setSearch, setPagination, pagination } = appContext;

  const handleSearchChange = ev => {
    setSearch(ev.target.value.trim());
    setPagination({
      ...pagination,
      currentPage: 1,
    });
  };

  return (
    <header>
      <input
        type="text"
        placeholder="Search by App"
        value={searchTerm}
        onChange={handleSearchChange}
      />
    </header>
  );
};

export default SearchBar;
