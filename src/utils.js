function prettifyUrl(str) {
  let encodedUrl = str.toString().toLowerCase();
  encodedUrl = encodedUrl.split(/[^a-z0-9]/).join('-');
  return encodedUrl;
}

export { prettifyUrl };
